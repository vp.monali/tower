#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct disk
{
  int data;
  struct disk*next;
};

struct disk*top1=NULL;
struct disk*top3=NULL;
struct disk*top2=NULL;

void tower1(char*,int);
void tower2(char*,int);
void tower3(char*,int);

void displyTower(struct disk*t)
{
	struct disk*temp=t;
	while(temp!=NULL)
	{
		printf("%d\n",temp->data);
		temp=temp->next;
	}
}

int main(int argc,char *argv[])
{
	struct disk*diskNode;
	struct disk*top=NULL;
	int i,*a;
	a=(int*)malloc(sizeof(int)*argc);
	for(i=1;i<argc;i++)
	{
		a[i]=atoi(argv[i]);
		diskNode=(struct disk*)malloc(sizeof(struct disk));
		diskNode->data=a[i];
		diskNode->next=NULL;
		if(top==NULL)
		{
			top=diskNode;
		}
		else
		{
			diskNode->next=top;
			top=diskNode;
		}
	}
	top1=top;
	printf("Before Moves Tower1\n");
	displyTower(top1);
	tower1("removeDisk",top1->data);      // removeDisk is an operation
	printf("\n\nAfter Moves Tower3\n");
	displyTower(top3);
	return 0;
}

void tower1(char*operationOnDisk,int data)
{
	struct disk*diskNode;
	if((strcmp(operationOnDisk,"removeDisk"))==0)
	{
		tower3("addDisk",top1->data);
		//		temp11=top1;
		top1=top1->next;
		//		free(temp11);
		tower2("addDisk",top1->data);
		//		temp12=top1;
		top1=top1->next;
		//		free(temp11);
		tower3("removeDisk",top3->data);
		tower3("addDisk",top1->data);
		//		temp13=top1;
		top1=top1->next;
		//		free(temp13);
		tower2("removeDisk",top2->data);
		tower3("addDisk",top1->data); 
		//		temp14=top1;
		//	top1=top1->next;
		//		free(temp14);
	}
	else
	{
		diskNode=(struct disk*)malloc(sizeof(struct disk));
		diskNode->data=data;
		printf("\nTower1  has Disk%d",diskNode->data);
		if(top1==NULL)
		{
			top1=diskNode;
		}
		else
		{
			diskNode->next=top1;
			top1=diskNode;
		}
	}
}//Tower1

void tower2(char*operationOnDisk,int data)
{
	struct disk*diskNode;
	if((strcmp(operationOnDisk,"removeDisk"))==0)
	{
		tower1("addDisk",top2->data);
		//		temp=top2;
		top2=top2->next;
		//		free(temp);
		tower3("addDisk",top2->data);
		//		temp1=top2;
		//	top2=top2->next;
		//		free(temp1);
	}
	else
	{
		diskNode=(struct disk*)malloc(sizeof(struct disk));
		diskNode->data=data;
		printf("\nTower2  has Disk%d",diskNode->data);
		if(top2==NULL)
		{
			top2=diskNode;
		}
		else
		{
			diskNode->next=top2;
			top2=diskNode;
		}
	}
}//end

void tower3(char*operationOnDisk,int data)
{
	struct disk*temp1,*temp,*diskNode;
	if((strcmp(operationOnDisk,"removeDisk"))==0)
	{
		tower2("addDisk",top3->data);
		//temp=top3;
		top3=top3->next;
		//		free(top3);
		//	        return top3;
	}
	else
	{										
		diskNode=(struct disk*)malloc(sizeof(struct disk));
		diskNode->data=data;
		printf("\nTower3 has  Disk%d",diskNode->data);
		if(top3==NULL)
		{
			top3=diskNode;
		}
		else
		{
			diskNode->next=top3;
			top3=diskNode;
		}
		//		return top3;
	}
}//end
